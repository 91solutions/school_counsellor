<?php
/**
 * @file
 * Contains \Drupal\ninetyone_school_compare\CustomClasses\SchoolComparisonManager.
 */

namespace Drupal\ninetyone_school_compare\CustomClasses;

use Drupal\entity_comparison\Entity\EntityComparison;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Render\Element;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Link;
use Drupal\Core\Url;

class SchoolComparisonManager {

  /**
   * Session service
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  /**
   * Current user service
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $current_user;

  /**
   * Constructor.
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   */
  public function __construct(Session $session, AccountProxyInterface $current_user) {
    $this->session = $session;
    $this->current_user = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session'),
      $container->get('current_user')
    );
  }

  public function removeComparisonSession(EntityComparison $entity_comparison) {
    // Get current user's id
    $uid = $this->current_user->id();

    // Get current entity comparison list
    $entity_comparison_list = $this->session->get('entity_comparison_' . $uid);

    //\Drupal::logger('removeComparisonSession')->debug('output <pre> @val', ['@val'=>print_r($entity_comparison_list,TRUE)]);
    // Get entity type and bundle type
    $entity_type = $entity_comparison->getTargetEntityType();
    $bundle_type = $entity_comparison->getTargetBundleType();

    if (!empty($entity_comparison_list)) {
      unset($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()]);
    }
    $this->session->set('entity_comparison_' . $uid, $entity_comparison_list);
  }

  public function removeComparisonSessionById(EntityComparison $entity_comparison, $entity_id) {
    // Get current user's id
    $uid = $this->current_user->id();

    // Get current entity comparison list
    $entity_comparison_list = $this->session->get('entity_comparison_' . $uid);

    // Get entity type and bundle type
    $entity_type = $entity_comparison->getTargetEntityType();
    $bundle_type = $entity_comparison->getTargetBundleType();

    if (!empty($entity_comparison_list)) {
      $comparison_ids = $entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()];
      foreach ($comparison_ids as $key => $id) {
        if ($id == $entity_id) {
          unset($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()][$key]);
        }
      }
    }
    $this->session->set('entity_comparison_' . $uid, $entity_comparison_list);
  }

  public function processRequest(EntityComparison $entity_comparison, $entity_id) {
    // Get current user's id
    $uid = $this->current_user->id();

    // Get entity type and bundle type
    $entity_type = $entity_comparison->getTargetEntityType();
    $bundle_type = $entity_comparison->getTargetBundleType();

    // Get current entity comparison list
    $entity_comparison_list = $this->session->get('entity_comparison_' . $uid);

    // Get entity
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);

    if ( empty($entity_comparison_list) ) {
      $add = TRUE;
    } else {
      if ( !empty($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()]) && in_array($entity_id, $entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()]) ) {
        $add = FALSE;
      } else {
        $add = TRUE;
      }
    }

    if ($add) {
      // Get the limit
      $limit = $entity_comparison->getLimit();

      // If the increased number of the list is lower or equal than the limit OR limit is 0 (no limit)
      if ( (isset($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()])
        && (count($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()]) + 1) <= $limit)
        || ($limit == 0)
        || (!isset($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()]) && $limit >= 1)) {

        // Add to the list
        $entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()][] = $entity_id;
        // drupal_set_message($this->t("You have successfully added %entity_name to %entity_comparison list.", array(
        //   '%entity_name' => $entity->label(),
        //   '%entity_comparison' => $entity_comparison->label(),
        // )));
      } else {
        drupal_set_message(t("You can only add @limit items to the %entity_comparison list.", array(
          '@limit' => $limit,
          '%entity_comparison' => $entity_comparison->label(),
        )), 'error');
      }

    } else{
      $key = array_search($entity_id, $entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()]);
      unset($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison->id()][$key]);
      // drupal_set_message($this->t("You have successfully removed %entity_name from %entity_comparison.", array(
      //   '%entity_name' => $entity->label(),
      //   '%entity_comparison' => $entity_comparison->label(),
      // )));
    }

    $this->session->set('entity_comparison_' . $uid, $entity_comparison_list);
  }

  /**
   * Compare page
   *
   * @return array
   */
  public function compare() {

    // Get the entity comparison id from the current path
    // $current_path = \Drupal::service('path.current')->getPath();
    // $current_path_array = explode('/', $current_path);
    $entity_comparison_id = 'school_comparison';

    // Declare table header and rows
    $header = array('');
    $rows = array();

    // Load the related entity comparison
    $entity_comparison = EntityComparison::load($entity_comparison_id);

    // Get current user's id
    $uid = $this->current_user->id();

    // Get entity type and bundle type
    $entity_type = $entity_comparison->getTargetEntityType();
    $bundle_type = $entity_comparison->getTargetBundleType();

    // Get the related entity view display
    $entity_view_display = EntityViewDisplay::load($entity_type . '.' . $bundle_type . '.' . $bundle_type . '_' . $entity_comparison_id);

    // Load field definitions
    $field_definitions = \Drupal::entityManager()->getFieldDefinitions($entity_comparison->getTargetEntityType(), $entity_comparison->getTargetBundleType());

    // Get fields
    $fields = $this->getTargetFields($field_definitions, $entity_view_display, $entity_comparison_id);

    // Get current entity comparison list
    $entity_comparison_list = $this->session->get('entity_comparison_' . $uid);

    $entities = array();

    if ( isset($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison_id]) ) {
      $count = 1;
      // Go through entities
      foreach ($entity_comparison_list[$entity_type][$bundle_type][$entity_comparison_id] as $entity_id) {
        $mobileDetector = \Drupal::service('mobile_detect');
        if($mobileDetector->isMobile()) {
          if ($count > 2) {
            break;
          } else {
            $count++;
          }
        }
        // Get entity
        $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
        // Get view builder
        $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);

        $entities[$entity_id] = array();

        // Add entity's label to the header
        $header[$entity_id] = $entity->toLink($entity->label());

        foreach ($fields as $field_name => $display_component) {
          if (isset($entity->{$field_name}) && $field = $entity->{$field_name}) {
            $field_renderable = $view_builder->viewField($field, $display_component);
            $entities[$entity_id][$field_name] = drupal_render($field_renderable);
            //$header[$entity_id] = $this->getRemoveLink($entity_id, $entity->toLink($entity->label()));
          }
        }
      }

      // If there are at least one entity in the list
      if ( count($entities) ) {
        // Add the first row, where user can remove the selected content from the list
        $row = array(t("Remove from the list"));
        foreach(Element::children($entities) as $key) {
          $row[] = $this->getRemoveLink($key);
        }
        $rows[] = $row;

        // Go through the selected fields
        foreach ($fields as $field_name => $display_component) {
          $mobileDetector = \Drupal::service('mobile_detect');
          if($mobileDetector->isMobile()) {
            if ($field_name == "field_school_image_gallery") {
              continue;
            }
          }
          // Set the field's label
          if ( is_a($field_definitions[$field_name], 'Drupal\field\Entity\FieldConfig') ) {
            // FieldConfig
            $row = array($field_definitions[$field_name]->label());
          } elseif ( is_a($field_definitions[$field_name], 'Drupal\Core\Field\BaseFieldDefinition') ) {
            //
            $row = array($field_definitions[$field_name]->getLabel());
          } else {
            // Do not write inse the first column
            $row = array('');
          }

          // Set the fields' values
          foreach (Element::children($entities) as $key) {
            $row[] = $entities[$key][$field_name];
             // \Drupal::logger('compare')->debug('output field_name <pre> @val, key @val2', ['@val'=>print_r($field_name,TRUE), '@val2'=>print_r($key,TRUE)]);
          }
          $rows[] = $row;
        }
      }
    }
    $mobileDetector = \Drupal::service('mobile_detect');
    if($mobileDetector->isMobile()) {
      return array(
        '#type' => 'table',
        '#rows' => $rows,
        '#header' => $header,
        '#empty' => t('No content available to compare.'),
        '#cache' => array(
          'max-age' => 0,
        ),
        '#sticky' => false,
        '#attributes' => ['class' => ['comparison-table', 'mobile-view']]
      );
    } else {
      return array(
        '#type' => 'table',
        '#rows' => $rows,
        '#header' => $header,
        '#empty' => t('No content available to compare.'),
        '#cache' => array(
          'max-age' => 0,
        ),
        '#sticky' => true,
        '#attributes' => ['class' => ['comparison-table']]
      );
    }
  }

  protected function getRemoveLink($entity_id, $str = null) {
    // Get the url object from route
    $url = Url::fromRoute('ninetyone_school_compare.remove_comparison_item', array(
      'entity_id' => $entity_id,
    ), array(
      'attributes' => array(
        'class' => 'use-ajax btn btn-default',
      ),
    ));

    // Set link text
    $link_text = t("Remove from the comparison");

    // Return with the link
    //return 'heading ' . \Drupal::l($link_text, $url);
    $link = Link::fromTextAndUrl($link_text, $url)->toString();
    //return t('@str @link', array('@str' => $str, '@link' => $link));
    return $link;
  }

  /**
   * Get target fields
   *
   * @param $field_definitions
   * @param \Drupal\Core\Entity\Entity\EntityViewDisplay $entity_view_display
   * @return array
   */
  protected function getTargetFields($field_definitions, EntityViewDisplay $entity_view_display, $entity_comparison_id) {
    $content_fields = $entity_view_display->get('content');

    $filtered_fields = array();

    foreach( $content_fields as $field_name => $field_settings ) {
      if ( isset($field_definitions[$field_name]) && isset($content_fields[$field_name]) && $field_definitions[$field_name]->isDisplayConfigurable('view') ) {
        $filtered_fields[$field_name] = $content_fields[$field_name];
      }
    }

    // Sort the fields by weight
    uasort($filtered_fields, 'Drupal\Component\Utility\SortArray::sortByWeightElement');

    return $filtered_fields;
  }

  public function getTermNamesByVid($vid) {
    $vocabulary_name = 'school_type';
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vocabulary_name);
    $query->sort('weight');
    $tids = $query->execute();
    $terms = Term::loadMultiple($tids);
    $output = [];
    foreach($terms as $term) {
        $output[$term->id()] = $term->getName();
    }
    return $output;
  }

  public function getSchools($type = NULL, $nid = NULL, $city = NULL) {
    $query = \Drupal::database()->select('node_field_data', 'n');
    $query->leftjoin('node__field_school_type','fst','n.nid = fst.entity_id');
    $query->leftjoin('node__field_address','nfa','n.nid = nfa.entity_id');
    $query->fields('fst', ['entity_id']);
    $query->fields('n', ['title']);
    if (!empty($type)) {
      $query->condition('fst.field_school_type_target_id',$type,'=');
    }
    if (!empty($city)) {
      $query->condition('nfa.field_address_locality',$city,'=');
    }
    $query->condition('n.status',1,'=');
    if (!empty($nid)) {
      $query->condition('n.nid',$nid,'NOT IN');
    }
    $result = $query->execute()->fetchAll();

    $output = [];
    foreach ($result as $school) {
      $output[$school->entity_id] = $school->title;
    }
    return $output;
  }

  public function getCity($type = NULL) {
    $query = \Drupal::database()->select('node_field_data', 'n');
    $query->leftjoin('node__field_school_type','fst','n.nid = fst.entity_id');
    $query->leftjoin('node__field_address','nfa','n.nid = nfa.entity_id');
    $query->fields('fst', ['entity_id']);
    $query->fields('n', ['title']);
    $query->fields('nfa', ['field_address_locality']);
    if (!empty($type)) {
      $query->condition('fst.field_school_type_target_id',$type,'=');
    }
    $query->condition('n.status',1,'=');
    $result = $query->execute()->fetchAll();

    $output = [];
    foreach ($result as $school) {
      $output[$school->field_address_locality] = $school->field_address_locality;
    }
    return $output;
  }

  public function prepareSchoolOptions($school_val = NULL, $triggered_element = NULL) {
    if (isset($school_val) && !empty($school_val)) {
      switch ($triggered_element) {
        case 'school1':
          $schools = [];
          if (!empty($school_val['school0'])) {
            $schools[] = $school_val['school0'];
          }
          return $schools;
          break;

        case 'school2':
          $schools = [];
          if (!empty($school_val['school0'])) {
            $schools[] = $school_val['school0'];
          }
          if (!empty($school_val['school1'])) {
            $schools[] = $school_val['school1'];
          }
          return $schools;
          break;

        case 'school3':
          $schools = [];
          if (!empty($school_val['school0'])) {
            $schools[] = $school_val['school0'];
          }
          if (!empty($school_val['school1'])) {
            $schools[] = $school_val['school1'];
          }
          if (!empty($school_val['school2'])) {
            $schools[] = $school_val['school2'];
          }
          return $schools;
          break;

        default:
          $schools = [];
          return $schools;
          break;
      }
    } else {
      $schools = [];
      return $schools;
    }
  }
}
