<?php
/**
 * @file
 * Contains \Drupal\ninetyone_school_compare\Controller\SchoolCompareController.
 */

namespace Drupal\ninetyone_school_compare\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ninetyone_school_compare\CustomClasses\SchoolComparisonManager;
use Drupal\entity_comparison\Entity\EntityComparison;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SchoolCompareController extends ControllerBase {

  /**
   * @var \Drupal\ninetyone_school_compare\CustomClasses\SchoolComparisonManager
   */
  protected $schoolComparisonManager;

  /**
   * Constructor.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct( SchoolComparisonManager $schoolComparisonManager) {
    $this->schoolComparisonManager = $schoolComparisonManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ninetyone_school_compare.manager')
    );
  }

  public function content() {
    return array(
      '#type'   => 'markup',
      '#markup' => t('Hello! It\'s working...'),
    );
  }

  /**
   * Remove item from comparison.
   *
   * @param $entity_id
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function removeComparisonItem($entity_id) {
    // Remove School Comparison session by entity id.
    $entity_comparison_id = 'school_comparison';
    $entity_comparison = EntityComparison::load($entity_comparison_id);
    $this->schoolComparisonManager->removeComparisonSessionById($entity_comparison, $entity_id);
    $output = $this->schoolComparisonManager->Compare();

    $response = new \Drupal\Core\Ajax\AjaxResponse();
    return $response->addCommand(new \Drupal\Core\Ajax\HtmlCommand('#js-ajax-data-wrapper', \Drupal::service('renderer')->render($output)));
  }
}
