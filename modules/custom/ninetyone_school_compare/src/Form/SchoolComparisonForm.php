<?php

/**
* @file
* Contains Drupal\ninetyone_school_compare\Form\SchoolComparisonForm
*/

namespace Drupal\ninetyone_school_compare\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_comparison\Entity\EntityComparison;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\ninetyone_school_compare\CustomClasses\SchoolComparisonManager;

/**
* School comparison form.
*/
class SchoolComparisonForm extends FormBase {

  /**
   * @var \Drupal\ninetyone_school_compare\CustomClasses\SchoolComparisonManager
   */
  protected $schoolComparisonManager;

  /**
   * Constructor.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(SchoolComparisonManager $schoolComparisonManager) {
    $this->schoolComparisonManager = $schoolComparisonManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ninetyone_school_compare.manager')
    );
  }

  public function getFormId() {
    return 'ninetyone_school_comparison_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    // Store comparison output.
    $output = $this->schoolComparisonManager->Compare();

    // \Drupal::logger('buildForm')->debug('output <pre>nid= @val', ['@val'=>print_r($form_state->getValues(),TRUE)]);

    $form['#prefix'] = '<div id="school_comparison_form_wrapper">';
    $form['#suffix'] = '</div>';
    $form['school_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select category'),
      '#weight' => 0,
      '#tree' => TRUE,
      '#prefix' => '<div class="form-field-wrapper"><div id="js-school-container">',
      '#suffix' => '</div>'
    ];

    $school_type = $this->schoolComparisonManager->getTermNamesByVid('school_type');
    $form['school_container']['school_type'] = [
      '#type' => 'select',
      //'#title' => $this->t('School Type'),
      '#required' => TRUE,
      '#options' => $school_type,
      '#multiple' => FALSE,
      '#default_value' => '',
      '#empty_option' => $this->t('Select School Type'),
      '#ajax' => [
        'callback' => '::updateSchoolListCallback',
        'event' => 'change',
        'progress' => array(
           'type' => 'throbber',
           'message' => t('Please wait.Fetching data...'),
          ),
        'wrapper' => 'school_comparison_form_wrapper',
       ],
      '#prefix' => '<div class="row"><div class="field-school-type col-sm-6">',
      '#suffix' => '</div>'
    ];

    $form['school_container']['school_city'] = [
      '#type' => 'select',
      //'#title' => $this->t('School City'),
      '#required' => TRUE,
      '#options' => $this->getCityOptions($form, $form_state),
      '#multiple' => FALSE,
      '#default_value' => '',
      '#empty_option' => $this->t('Select City'),
      '#ajax' => [
        'callback' => '::updateSchoolListCallback',
        'event' => 'change',
        'progress' => array(
           'type' => 'throbber',
           'message' => t('Please wait.Fetching data...'),
          ),
        'wrapper' => 'js-ajax-elements-wrapper',
       ],
       '#states' => array(
        'enabled' => array(
          'select[name="school_container[school_type]"]' => array('!value' => ''),
        ),
      ),
      '#prefix' => '<div class="field-school-city col-sm-6">',
      '#suffix' => '</div></div>'
    ];

    $form['field_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Schools for comparison ( select minimum of two schools )'),
      '#weight' => 0,
      '#tree' => TRUE,
      // Set up the wrapper so that AJAX will be able to replace the fieldset.
      '#prefix' => '<div id="js-ajax-elements-wrapper">',
      '#suffix' => '</div>',
    ];
    $mobileDetector = \Drupal::service('mobile_detect');
    if($mobileDetector->isMobile()) {
      $form['field_container']['#title'] = $this->t('Select schools');
    }

    $triggered_element = $form_state->getTriggeringElement();
    $get_form_state_val = $form_state->getValues();

    $form['field_container']['school0'] = [
      '#type' => 'select',
      //'#title' => $this->t('School'),
      '#required' => TRUE,
      '#options' => $this->getSchoolOptions($form, $form_state),
      '#multiple' => FALSE,
      '#default_value' => '',
      '#empty_option' => $this->t('Select School'),
      '#ajax' => [
        'callback' => '::updateSchoolListCallback',
        'event' => 'change',
        'progress' => array(
           'type' => 'throbber',
           'message' => t('Please wait.Fetching data...'),
          ),
        'wrapper' => 'js-ajax-elements-wrapper',
       ],
      '#states' => array(
        'enabled' => array(
          'select[name="school_container[school_city]"]' => array('!value' => ''),
        ),
      ),
      '#prefix' => '<div id="js-ajax-school0-wrapper" class="form-group col-sm-3">',
      '#suffix' => '</div>',
      //'#disabled' => FALSE,
    ];

    $selected_school_arr = isset($get_form_state_val['field_container'])?$get_form_state_val['field_container']:NULL;

    $form['field_container']['school1'] = [
      '#type' => 'select',
      //'#title' => $this->t('School'),
      '#required' => TRUE,
      '#options' => $this->getSchoolOptions($form, $form_state, $this->schoolComparisonManager->prepareSchoolOptions($selected_school_arr, 'school1')),
      '#multiple' => FALSE,
      '#default_value' => '',
      '#empty_option' => $this->t('Select School'),
      //'#disabled' => TRUE,
      '#states' => array(
        'enabled' => array(
          'select[name="field_container[school0]"]' => array('!value' => ''),
        ),
      ),
      '#ajax' => [
        'callback' => '::updateSchoolListCallback',
        'event' => 'change',
        'progress' => array(
           'type' => 'throbber',
           'message' => t('Please wait.Fetching data...'),
          ),
        'wrapper' => 'js-ajax-elements-wrapper',
       ],
      '#prefix' => '<div id="js-ajax-school1-wrapper" class="form-group col-sm-3">',
      '#suffix' => '</div>',
    ];
    $mobileDetector = \Drupal::service('mobile_detect');
    if(!$mobileDetector->isMobile()) {
      $form['field_container']['school2'] = [
        '#type' => 'select',
        //'#title' => $this->t('School'),
        '#required' => FALSE,
        '#options' => $this->getSchoolOptions($form, $form_state, $this->schoolComparisonManager->prepareSchoolOptions($selected_school_arr, 'school2')),
        '#multiple' => FALSE,
        '#default_value' => '',
        '#empty_option' => $this->t('Select School'),
        '#states' => array(
          'enabled' => array(
            'select[name="field_container[school1]"]' => array('!value' => ''),
          ),
        ),
        '#ajax' => [
          'callback' => '::updateSchoolListCallback',
          'event' => 'change',
          'progress' => array(
             'type' => 'throbber',
             'message' => t('Please wait.Fetching data...'),
            ),
          'wrapper' => 'js-ajax-elements-wrapper',
         ],
        '#prefix' => '<div id="js-ajax-school2-wrapper" class="form-group col-sm-3">',
        '#suffix' => '</div>',
      ];
      $form['field_container']['school3'] = [
        '#type' => 'select',
        //'#title' => $this->t('School'),
        '#required' => FALSE,
        '#options' => $this->getSchoolOptions($form, $form_state, $this->schoolComparisonManager->prepareSchoolOptions($selected_school_arr, 'school3')),
        '#multiple' => FALSE,
        '#default_value' => '',
        '#empty_option' => $this->t('Select School'),
        '#states' => array(
          'enabled' => array(
            'select[name="field_container[school2]"]' => array('!value' => ''),
          ),
        ),
        '#ajax' => [
          'callback' => '::updateSchoolListCallback',
          'event' => 'change',
          'progress' => array(
             'type' => 'throbber',
             'message' => t('Please wait.Fetching data...'),
            ),
          'wrapper' => 'js-ajax-elements-wrapper',
         ],
        '#prefix' => '<div id="js-ajax-school3-wrapper" class="form-group col-sm-3">',
        '#suffix' => '</div>',
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 1
    ];

    $form_state->setCached(FALSE);

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Compare'),
      //'#disabled' => TRUE,
      '#validate' => array('::validateComparisonForm'),
      '#states' => array(
        'enabled' => array(
          'select[name="field_container[school0]"]' => array('!value' => ''),
          'select[name="field_container[school1]"]' => array('!value' => ''),
        ),
      ),
       '#submit' => array('::submitForm'),
      // '#ajax' => [
      //   'callback' => '::submitForm',
      //   'wrapper' => 'js-ajax-data-wrapper',
      // ],
      '#weight' => 1,
      '#suffix' => '</div>',
    ];

    $no_data = (empty($output)) ? "no-data" : "";
    $form['data_container'] = [
      '#type' => 'fieldset',
      '#weight' => 2,
      '#tree' => TRUE,
      // Set up the wrapper so that AJAX will be able to replace the fieldset.
      '#prefix' => '<div id="js-ajax-data-wrapper" class="' . $no_data . '">',
      '#suffix' => '</div>',
    ];

    $form['data_container']['data_output'] = [
      '#markup' => \Drupal::service('renderer')->render($output),
    ];

    return $form;

  }

  public function updateSchoolListCallback(array &$form, FormStateInterface &$form_state) {
    $element = $form_state->getTriggeringElement();

    // $selected_school_nid = $element['#value'];
    // $schools = $this->schoolComparisonManager->getSchools(1, $selected_school_nid);
    // \Drupal::logger('updateSchoolListCallback')->debug('output <pre>nid= @val, list=@val2', ['@val'=>print_r($element,TRUE), '@val2'=>print_r($schools,TRUE)]);

    // $form_state->set('field_deltas', $schools);

    //$form['field_container']['school0']['#disabled'] = TRUE;

    $form_state->setRebuild();

    if ($element['#name'] == 'school_container[school_type]') {
      return $form;
    }

    return $form['field_container'];
  }

  public function validateComparisonForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_comparison_id = 'school_comparison';
    $entity_comparison = EntityComparison::load($entity_comparison_id);
    // Remove School Comparison session.
    $this->schoolComparisonManager->removeComparisonSession($entity_comparison);

    for ($i=0; $i < 4 ; $i++) {
      $nid = $form_state->getValue(['field_container', 'school'.$i]);
      if (isset($nid) && !empty($nid)) {
        $this->schoolComparisonManager->processRequest($entity_comparison, $nid);
        // \Drupal::logger('submitForm')->debug('output <pre> @val', ['@val'=>print_r($nid,TRUE)]);
      }
    }
    // $output = $this->schoolComparisonManager->Compare();

    // $form_state->setValue(['data_container', 'data_output', 'markup'], $output);

    // $form_state->setRebuild();

    // // $form_state->setRedirect('entity_comparison.compare', ['entity_comparison_id'=>$entity_comparison_id]);
    // $response = new \Drupal\Core\Ajax\AjaxResponse();
    // $response->addCommand(new \Drupal\Core\Ajax\HtmlCommand('#js-ajax-data-wrapper', $output));
    // return $response;
  }

  public function getCityOptions(array &$form, FormStateInterface $form_state, $selected_schools_type = NULL) {
    $element = $form_state->getTriggeringElement();

    $get_form_state_val = $form_state->getValues();
    if (!empty($get_form_state_val)) {
      if (isset($get_form_state_val['school_container']['school_type']) && !empty($get_form_state_val['school_container']['school_type'])) {
        $schools = $this->schoolComparisonManager->getCity($get_form_state_val['school_container']['school_type']);
      } else {
        $schools = $this->schoolComparisonManager->getCity();
      }
    } else {
      $schools = $this->schoolComparisonManager->getCity();
    }
    return $schools;
  }

  public function getSchoolOptions(array &$form, FormStateInterface $form_state, $selected_schools = NULL) {
    $element = $form_state->getTriggeringElement();

    $get_form_state_val = $form_state->getValues();
    if (!empty($get_form_state_val)) {
      if (isset($get_form_state_val['school_container']['school_type']) && !empty($get_form_state_val['school_container']['school_type'])) {
        if (isset($get_form_state_val['school_container']['school_city']) && !empty($get_form_state_val['school_container']['school_city'])) {
          $schools = $this->schoolComparisonManager->getSchools($get_form_state_val['school_container']['school_type'], $selected_schools, $get_form_state_val['school_container']['school_city']);
        } else {
          $schools = $this->schoolComparisonManager->getSchools($get_form_state_val['school_container']['school_type'], $selected_schools);
        }
      } else {
        $schools = $this->schoolComparisonManager->getSchools(NULL, $selected_schools);
      }
    } else {
      $schools = $this->schoolComparisonManager->getSchools();
    }
    return $schools;
  }

}
