/**
 * @file
 * Node Delete confirmation file.
 */

(function($) {

  Drupal.testAjax = {
    // Our form
    form_id: 'ninetyone_school_comparison_form'  //Yes, I tested on my extended node creation form
  };

  Drupal.behaviors.testAjax = {
    attach: function(context, settings) {

      // We extend Drupal.ajax objects for all AJAX elements in our form
      // for (ajax_el in settings.ajax) {
      //   alert(ajax_el);
      //   if (Drupal.ajax['edit-school-container-school-type'].element.form) {
      //     if (Drupal.ajax['edit-school-container-school-type'].element.form.id === Drupal.testAjax.form_id) {
      //       Drupal.ajax['edit-school-container-school-type'].beforeSubmit = Drupal.testAjax.beforeSubmit;
      //       Drupal.ajax['edit-school-container-school-type'].success = Drupal.testAjax.success;
      //       Drupal.ajax['edit-school-container-school-type'].error = Drupal.testAjax.error;
      //     }
      //   }
      // }
      Drupal.ajax['edit-school-container-school-type'].options.beforeSubmit = Drupal.testAjax.beforeSubmit;
      Drupal.ajax['edit-school-container-school-type'].options.success = Drupal.testAjax.success;
      Drupal.ajax['edit-school-container-school-type'].options.error = Drupal.testAjax.error;
    }
  };

  // Disable form
  Drupal.testAjax.beforeSubmit = function (form_values, form, options) {
    $(form[0].elements).not(':disabled')
                       .addClass('test-ajax-disabled')
                       .attr('disabled', true);
  }

  // Enable form
  Drupal.testAjax.enableForm = function(form) {
    $(form).find('.test-ajax-disabled')
            .removeClass('test-ajax-disabled')
            .attr('disabled', false);

  }

  Drupal.testAjax.success = function (response, status) {
    Drupal.testAjax.enableForm(this.element.form);
    // Call original method with main functionality
    Drupal.ajax.prototype.success.call(this, response, status);
  }
  Drupal.testAjax.error = function (response, uri) {
    Drupal.testAjax.enableForm(this.element.form);
    // Call original method with main functionality
    Drupal.ajax.prototype.error.call(this, response, uri);
  }

})(jQuery);
