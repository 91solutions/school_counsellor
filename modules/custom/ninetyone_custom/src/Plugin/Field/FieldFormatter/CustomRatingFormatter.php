<?php
/**
 * @file
 * Contains \Drupal\ninetyone_custom\Plugin\Field\FieldFormatter\CustomRatingFormatter.
 */

namespace Drupal\ninetyone_custom\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'custom_rating_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_rating_formatter",
 *   label = @Translation("Custom Rating formatter"),
 *   field_types = {
 *     "voting_api_field"
 *   }
 * )
 */

class CustomRatingFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    $summary[] = t('Displays the custom rating.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();
    $field_settings = $this->getFieldSettings();
    $field_name = $this->fieldDefinition->getName();

    $vote_type = $field_settings['vote_type'];
    $vote_plugin = $field_settings['vote_plugin'];

    $query = \Drupal::database()->select('votingapi_result', 'vr');
    $query->fields('vr', ['value']);
    $query->condition('vr.entity_type','node','=');
    $query->condition('vr.entity_id',$entity->id(),'=');
    $query->condition('vr.function','vote_field_average:node.field_rating','=');
    $result = $query->execute()->fetchCol();

    \Drupal::logger('custom plugin')->debug('output <pre> @val', ['@val'=>print_r($result,TRUE)]);

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = [
        '#theme' => 'custom_rating_formatter',
        '#entity_id' => $entity->id(),
        '#result' => (!empty($result))?floor($result[0]) : 0,
        '#no_stars' => (!empty($result))?(5 - floor($result[0])):5,
      ];
    }

    return $element;
  }
}
