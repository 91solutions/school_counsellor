<?php

namespace Drupal\ninetyone_custom\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\Xss;

/**
 * Subscribe to kernal request event to check authentication.
 */
class CustomSubscriber implements EventSubscriberInterface {

  /**
   * Redirect non-authenticated users to login page.
   */
  public function checkUserRedirect(GetResponseEvent $event) {
    global $base_url;
    $current_path = \Drupal::service('path.current')->getPath();
    $patterns = "/node/add/school\n/add-new-school";
    if ($match = \Drupal::service('path.matcher')->matchPath($current_path, $patterns)) {
      // Prepare authentication redirect path.
      $redirect = $base_url . '/user/login?destination=add-new-school';
      if (\Drupal::currentUser()->isAnonymous()) {
        $message = t('Please login or register to add new school.');
        drupal_set_message($message, 'warning');
        //$event->setResponse(new RedirectResponse($redirect));
      }
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkUserRedirect'];
    return $events;
  }

}
