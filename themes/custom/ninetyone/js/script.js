(function($) {
  Drupal.behaviors.ninetyone = {
    attach: function (context, settings) {
      //Check to see if the window is top if not then display button
      $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
          $('.scrolltotop').fadeIn();
        } else {
          $('.scrolltotop').fadeOut();
        }

        /*multiplier = 10000.75;
        var from_top = $(this).scrollTop(),
        bg_css = '0px ' + ((3 * (multiplier / from_top))) + 'px';
        $(".rate_school").css({"background-position" : bg_css });
        $(".blog_content").css({"background-position" : bg_css });*/
      });

      //Click event to scroll to top
      $('.scrolltotop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
      });

      new WOW().init();

      jQuery(".user-logged-in a.admin-hide").parents('li').css("display", "none");

      var tmp = jQuery(".marquee").find(".view-empty");
      if(tmp.length) {
          jQuery(".marquee").removeClass("marquee");
      } else {
        $('.marquee').marquee({
          //duration in milliseconds of the marquee
          duration: 10000,
          //gap in pixels between the tickers
          gap: 100,
          //time in milliseconds before the marquee will start animating
          delayBeforeStart: 0,
          //'left' or 'right'
          direction: 'up',
          //true or false - should the marquee be duplicated to show an effect of continues flow
          duplicated: true,
          pauseOnHover: true
        });
      }
    }
  }
})(jQuery);
